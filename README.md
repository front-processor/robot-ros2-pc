## compile
colcon build
source install/setup.bash

## navication module
ros2 launch fishbot_navigation2 navigation2.launch.py

## build map module
ros2 launch fishbot_cartographer cartographer.launch.py

## save map
ros2 run nav2_map_server map_saver_cli -t map -f fishbot_map

## gnss module
ros2 launch fdilink_ahrs ahrs_driver.launch.py

## lidar module
ros2 launch sllidar_ros2 sllidar_a3_launch.py

## auto install dependency
rosdepc install --from-path src --ignore-src -r -y
